INSERT INTO setting(name, description) VALUES('Tronix', 'Cyberpunk');

INSERT INTO race(name, "settingId") VALUES('Old Worlder', 1);
INSERT INTO race(name, "settingId") VALUES('Peasant', 1);
INSERT INTO race(name, "settingId") VALUES('Tecchi', 1);
INSERT INTO race(name, "settingId") VALUES('Noble', 1);
INSERT INTO race(name, "settingId") VALUES('Patrician', 1);
INSERT INTO class(name, "settingId") VALUES('Symbient', 1);
INSERT INTO class(name, "settingId") VALUES('Juicer', 1);
INSERT INTO class(name, "settingId") VALUES('Cipher', 1);
INSERT INTO class(name, "settingId") VALUES('Machinist', 1);

INSERT INTO stat(name, "settingId") VALUES('Strength', 1); 
INSERT INTO stat(name, "settingId") VALUES('Dexterity', 1); 
INSERT INTO stat(name, "settingId") VALUES('Constitution', 1); 
INSERT INTO stat(name, "settingId") VALUES('Endurance', 1); 
INSERT INTO stat(name, "settingId") VALUES('Intelligence', 1); 
INSERT INTO stat(name, "settingId") VALUES('Wisdom', 1); 
INSERT INTO stat(name, "settingId") VALUES('Charisma', 1); 

INSERT INTO skill(name, "statId", "settingId") VALUES('Heavy Guns', 1, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Unarmed', 1, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Melee Weapons', 1, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Throwing', 1, 1);

INSERT INTO skill(name, "statId", "settingId") VALUES('Precision Guns', 2, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Unarmed', 2, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Sneak', 2, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Lockpick', 2, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Steal', 2, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Melee Weapons', 2, 1);

INSERT INTO skill(name, "statId", "settingId") VALUES('Energy Weapons', 5, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Hacking', 5, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Traps', 5, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Engineering', 5, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Repair', 5, 1);

INSERT INTO skill(name, "statId", "settingId") VALUES('Personal Piloting', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Freighter Piloting', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Diagnostics', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('First Aid', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Engineering', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Repair', 6, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Surgery', 6, 1);

INSERT INTO skill(name, "statId", "settingId") VALUES('Persuasion', 7, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Intimidation', 7, 1);
INSERT INTO skill(name, "statId", "settingId") VALUES('Trade', 7, 1);

INSERT INTO "user"(email, uid) VALUES('Jeffy@amazon.com', 'Zv1NPmF8jjXJzkOZs0ceL4gfyPT2');

