import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import CreateUserDto from 'src/models/dtos/CreateUserDto';
import { UserService } from './user.service';

@Controller('api/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getUser(@Query() queryParams: { uid: string }) {
    console.log(`params : ${JSON.stringify(queryParams)}`);
    return this.userService.getUser(queryParams.uid);
  }

  @Post()
  insert(@Body() user: CreateUserDto): Promise<Object> {
    return this.userService.insertUser(user);
  }
}
