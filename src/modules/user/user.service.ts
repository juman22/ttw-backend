import { Injectable } from '@nestjs/common';
import CreateUserDto from 'src/models/dtos/CreateUserDto';
import { UserDao } from 'src/persistence/daos/user.dao';

@Injectable()
export class UserService {
  constructor(private readonly userDao: UserDao) {}

  insertUser(user: CreateUserDto): Promise<Object> {
    return this.userDao.insertUser(user);
  }

  getUser(uid: string) {
    return this.userDao.getUser(uid);
  }
}
