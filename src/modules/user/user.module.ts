import { Module } from '@nestjs/common';
import { UserDao } from 'src/persistence/daos/user.dao';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  controllers: [UserController],
  providers: [UserService, UserDao],
})
export class UserModule {}
