import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import Equipment from 'src/persistence/models/setting/equipment.model';
import Item from 'src/persistence/models/setting/item.model';
import StatusEffect from 'src/persistence/models/setting/status.effect.model';
import { SettingService } from './setting.service';

@Controller('api/setting')
export class SettiingController {
  constructor(private readonly settingService: SettingService) {}

  @Get()
  public async getAllSettings() {
    return this.settingService.getAllSettings();
  }

  @Get('class')
  public async getAllClasses() {
    return this.settingService.getAllClasses();
  }

  @Get('class')
  public async getClassBySetting(@Query() queryParams: { settingId: number }) {
    return this.settingService.getClassesBySetting(queryParams.settingId);
  }

  @Get('race')
  public async getAllRaces() {
    return this.settingService.getAllRaces();
  }

  @Get('race')
  public async getRacesBySetting(@Query() queryParams: { settingId: number }) {
    return this.settingService.getRacesBySetting(queryParams.settingId);
  }

  @Get('skill')
  public async getAllSkills() {
    return this.settingService.getAllSkills();
  }

  @Get('skill')
  public async getSkillsBySetting(@Query() queryParams: { settingId: number }) {
    return this.settingService.getSkillsBySetting(queryParams.settingId);
  }

  @Get('stat')
  public async getStatsBySetting(@Query() queryParams: { settingId: number }) {
    return this.settingService.getStatsBySetting(queryParams.settingId);
  }

  @Get('equipment')
  public async getEquipmentsBySetting(
    @Query() queryParams: { settingId: number },
  ) {
    return this.settingService.getEquipmentsBySetting(queryParams.settingId);
  }

  @Get('search/equipment')
  public async searchEquipmentBySetting(
    @Query() queryParams: { settingId: number; searchString: string },
  ) {
    console.log(`searching string : ${queryParams.searchString}`);
    return this.settingService.searchEquipmentBySetting(
      queryParams.settingId,
      queryParams.searchString,
    );
  }

  @Get('search/statusEffects')
  public async searchStatusEffectsBySetting(
    @Query() queryParams: { settingId: number; searchString: string },
  ) {
    console.log(`searching string : ${queryParams.searchString}`);
    return this.settingService.searchStatusEffectsBySetting(
      queryParams.settingId,
      queryParams.searchString,
    );
  }

  @Get('search/items')
  public async searchItemsBySetting(
    @Query() queryParams: { settingId: number; searchString: string },
  ) {
    console.log(`searching string : ${queryParams.searchString}`);
    return this.settingService.searchItemsBySetting(
      queryParams.settingId,
      queryParams.searchString,
    );
  }

  @Get('item')
  public async getItemsBySetting(@Query() queryParams: { settingId: number }) {
    return this.settingService.getItemsBySetting(queryParams.settingId);
  }

  @Get('statusEffect')
  public async getStatusEffectsBySetting(
    @Query() queryParams: { settingId: number },
  ) {
    return this.settingService.getStatusEffectsBySetting(queryParams.settingId);
  }

  @Post('item')
  public async createItem(@Body() item: Item) {
    console.log(`CREATING ITEM ${JSON.stringify(item)}`);
    return this.settingService.insertItem(item);
  }

  @Post('equipment')
  public async createEquipment(@Body() equipment: Equipment) {
    console.log(`CREATING EQUIPMENT ${JSON.stringify(equipment)}`);
    return this.settingService.insertEquipment(equipment);
  }

  @Post('statusEffect')
  public async createStatusEffect(@Body() statusEffect: StatusEffect) {
    console.log(`CREATING STATUS EFFECT ${JSON.stringify(statusEffect)}`);
    return this.settingService.insertStatusEffect(statusEffect);
  }
}
