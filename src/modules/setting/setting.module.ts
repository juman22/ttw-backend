import { Module } from '@nestjs/common';
import { ClassDao } from 'src/persistence/daos/class.dao';
import { EquipmentDao } from 'src/persistence/daos/equipment.dao';
import { StatModifierDao } from 'src/persistence/daos/stat.modifier.dao';
import { RaceDao } from 'src/persistence/daos/race.dao';
import { SettingDao } from 'src/persistence/daos/setting.dao';
import { SkillDao } from 'src/persistence/daos/skill.dao';
import { StatDao } from 'src/persistence/daos/stat.dao';
import { StatusEffectDao } from 'src/persistence/daos/status.effect.dao';
import { SettiingController } from './setting.controller';
import { SettingService } from './setting.service';
import { SkillModifierDao } from 'src/persistence/daos/skill.modifier.dao';
import { UniversalModifierDao } from 'src/persistence/daos/universal.modifier.dao';
import { ItemDao } from 'src/persistence/daos/item.dao';

@Module({
  controllers: [SettiingController],
  providers: [
    SettingService,
    SettingDao,
    RaceDao,
    ClassDao,
    SkillDao,
    StatDao,
    EquipmentDao,
    StatusEffectDao,
    StatModifierDao,
    SkillModifierDao,
    UniversalModifierDao,
    ItemDao,
  ],
})
export class SettingModule {}
