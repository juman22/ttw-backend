import { Injectable } from '@nestjs/common';
import { ClassDao } from 'src/persistence/daos/class.dao';
import { EquipmentDao } from 'src/persistence/daos/equipment.dao';
import { ItemDao } from 'src/persistence/daos/item.dao';
import { RaceDao } from 'src/persistence/daos/race.dao';
import { SettingDao } from 'src/persistence/daos/setting.dao';
import { SkillDao } from 'src/persistence/daos/skill.dao';
import { SkillModifierDao } from 'src/persistence/daos/skill.modifier.dao';
import { StatDao } from 'src/persistence/daos/stat.dao';
import { StatModifierDao } from 'src/persistence/daos/stat.modifier.dao';
import { StatusEffectDao } from 'src/persistence/daos/status.effect.dao';
import { UniversalModifierDao } from 'src/persistence/daos/universal.modifier.dao';
import Equipment from 'src/persistence/models/setting/equipment.model';
import Item from 'src/persistence/models/setting/item.model';
import StatusEffect from 'src/persistence/models/setting/status.effect.model';

@Injectable()
export class SettingService {
  constructor(
    private readonly settingDao: SettingDao,
    private readonly classDao: ClassDao,
    private readonly raceDao: RaceDao,
    private readonly skillDao: SkillDao,
    private readonly statDao: StatDao,
    private readonly equipmentDao: EquipmentDao,
    private readonly itemDao: ItemDao,
    private readonly statusEffectDao: StatusEffectDao,
    private readonly statModifierDao: StatModifierDao,
    private readonly skillModifierDao: SkillModifierDao,
    private readonly universalModifierDao: UniversalModifierDao,
  ) {}

  public async getAllSettings() {
    return this.settingDao.getAllSettings();
  }

  public async getAllClasses() {
    return this.classDao.getAllClasses();
  }

  public async getClassesBySetting(settingId: number) {
    return this.classDao.getClassesBySetting(settingId);
  }

  public async getAllRaces() {
    return this.raceDao.getAllRaces();
  }

  public async getRacesBySetting(settingId: number) {
    return this.raceDao.getRacesBySetting(settingId);
  }

  public async getAllSkills() {
    return this.skillDao.getAllSkills();
  }

  public async getSkillsBySetting(settingId: number) {
    return this.skillDao.getSkillsBySetting(settingId);
  }

  public async getStatsBySetting(settingId: number) {
    return this.statDao.getStatsBySetting(settingId);
  }

  public async getEquipmentsBySetting(settingId: number) {
    return this.equipmentDao.getEquipmentsBySetting(settingId);
  }

  public async searchEquipmentBySetting(
    settingId: number,
    searchString: string,
  ) {
    return this.equipmentDao.searchEquipmentBySetting(settingId, searchString);
  }

  public async searchStatusEffectsBySetting(
    settingId: number,
    searchString: string,
  ) {
    return this.statusEffectDao.searchStatusEffectsBySetting(
      settingId,
      searchString,
    );
  }

  public async searchItemsBySetting(settingId: number, searchString: string) {
    return this.itemDao.searchItemsBySetting(settingId, searchString);
  }

  public async getItemsBySetting(settingId: number) {
    return this.itemDao.getItemsBySetting(settingId);
  }

  public async getStatusEffectsBySetting(settingId: number) {
    return this.statusEffectDao.getStatusEffectsBySetting(settingId);
  }

  public async insertItem(item: Item) {
    return this.itemDao.insertItem(item);
  }

  public async insertEquipment(equipment: Equipment) {
    const statModifierSaveResponse = await this.statModifierDao.insertStatModifiers(
      equipment.statModifiers,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(statModifierSaveResponse)}`,
    );
    const skillModifierSaveResponse = await this.skillModifierDao.insertSkillModifiers(
      equipment.skillModifiers,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(skillModifierSaveResponse)}`,
    );
    const universalModifierSaveResponse = await this.universalModifierDao.insertUniversalModifier(
      equipment.universalModifier,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(universalModifierSaveResponse)}`,
    );
    equipment.statModifiers = statModifierSaveResponse;
    equipment.skillModifiers = skillModifierSaveResponse;
    equipment.universalModifier = universalModifierSaveResponse;
    return this.equipmentDao.insertEquipment(equipment);
  }

  public async insertStatusEffect(statusEffect: StatusEffect) {
    const statModifierSaveResponse = await this.statModifierDao.insertStatModifiers(
      statusEffect.statModifiers,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(statModifierSaveResponse)}`,
    );
    const skillModifierSaveResponse = await this.skillModifierDao.insertSkillModifiers(
      statusEffect.skillModifiers,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(skillModifierSaveResponse)}`,
    );
    const universalModifierSaveResponse = await this.universalModifierDao.insertUniversalModifier(
      statusEffect.universalModifier,
    );
    console.log(
      `MODIFIER SAVE RESPONSE ${JSON.stringify(universalModifierSaveResponse)}`,
    );
    statusEffect.statModifiers = statModifierSaveResponse;
    statusEffect.skillModifiers = skillModifierSaveResponse;
    statusEffect.universalModifier = universalModifierSaveResponse;
    return this.statusEffectDao.insertStatusEffect(statusEffect);
  }
}
