import { Module } from '@nestjs/common';
import { CharacterDao } from 'src/persistence/daos/character.dao';
import { CharacterEquipmentDao } from 'src/persistence/daos/character.equipment.dao';
import { CharacterSkillDao } from 'src/persistence/daos/character.skill.dao';
import { CharacterStatDao } from 'src/persistence/daos/character.stat.dao';
import { CharacterEffectDao } from 'src/persistence/daos/character.effect.dao';
import { CharacterController } from './character.controller';
import { CharacterService } from './character.service';
import { CharacterItemDao } from 'src/persistence/daos/character.item.dao';

@Module({
  controllers: [CharacterController],
  providers: [
    CharacterService,
    CharacterDao,
    CharacterSkillDao,
    CharacterStatDao,
    CharacterEquipmentDao,
    CharacterEffectDao,
    CharacterItemDao,
  ],
})
export class CharacterModule {}
