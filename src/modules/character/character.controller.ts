import { Body, Controller, Delete, Get, Post, Query } from '@nestjs/common';
import CreateCharacterDto from 'src/models/dtos/CreateCharacterDto';
import CreateCharacterSkillDto from 'src/models/dtos/CreateCharacterSkillDto';
import CreateCharacterStatDto from 'src/models/dtos/CreateCharacterStatDto';
import CreateCharacterStatusEffectDto from 'src/models/dtos/CreateCharacterStatusEffectDto';
import InsertCharacterEquipmentDto from 'src/models/dtos/InsertCharacterEquipmentDto';
import InsertCharacterItemDto from 'src/models/dtos/InsertCharacterItemDto';
import UpdateCharacterDto from 'src/models/dtos/UpdateCharacterDto';
import UpdateCharacterEquipmentDto from 'src/models/dtos/UpdateCharacterEquipmentDto';
import UpdateCharacterItemDto from 'src/models/dtos/UpdateCharacterItemDto';
import UpdateCharacterSkillDto from 'src/models/dtos/UpdateCharacterSkillDto';
import { CharacterService } from './character.service';

@Controller('api/character')
export class CharacterController {
  constructor(private readonly characterService: CharacterService) {}

  @Post()
  insert(@Body() character: CreateCharacterDto): Promise<Object> {
    return this.characterService.insertCharacter(character);
  }

  @Post('update')
  update(@Body() character: UpdateCharacterDto): Promise<Object> {
    return this.characterService.updateCharacter(character);
  }

  @Post('skill')
  insertSkills(@Body() createCharacterSkillDtos: CreateCharacterSkillDto[]) {
    return this.characterService.insertSkills(createCharacterSkillDtos);
  }

  @Post('skill/update')
  updateSkill(
    @Body() characterSkillDto: UpdateCharacterSkillDto,
  ): Promise<Object> {
    return this.characterService.updateSkill(characterSkillDto);
  }

  @Post('stat')
  insertStats(@Body() createCharacterStatDtos: CreateCharacterStatDto[]) {
    return this.characterService.insertStats(createCharacterStatDtos);
  }

  @Post('equipment')
  insertCharacterEquipment(
    @Body() insertCharacterEquipmentDto: InsertCharacterEquipmentDto,
  ) {
    return this.characterService.insertCharacterEquipment(
      insertCharacterEquipmentDto,
    );
  }

  @Post('item')
  insertCharacterItem(@Body() insertCharacterItemDto: InsertCharacterItemDto) {
    return this.characterService.insertCharacterItem(insertCharacterItemDto);
  }

  @Get('equipment')
  getEquipmentByCharacter(@Query() queryParams: { characterId: number }) {
    return this.characterService.getEquipmentByCharacterId(
      queryParams.characterId,
    );
  }

  @Get('item')
  getItemsByCharacter(@Query() queryParams: { characterId: number }) {
    return this.characterService.getItemsByCharacterId(queryParams.characterId);
  }

  @Post('item/quantity')
  updateItemQuantity(@Body() updateCharacterItemDto: UpdateCharacterItemDto) {
    return this.characterService.updateCharacterItem(updateCharacterItemDto);
  }

  @Post('equipment/equip')
  equipItem(@Body() updateEquipmentDto: UpdateCharacterEquipmentDto) {
    return this.characterService.equipItem(updateEquipmentDto);
  }

  @Post('equipment/unequip')
  unequipItem(@Body() updateEquipmentDto: UpdateCharacterEquipmentDto) {
    return this.characterService.unequipItem(updateEquipmentDto);
  }

  @Get()
  getCharacter(@Query() queryParams: { characterId: number }) {
    return this.characterService.getCharacter(queryParams.characterId);
  }

  @Get('user')
  getCharactersByUser(@Query() queryParams: { userId: number }) {
    console.log(`${JSON.stringify(queryParams)}`);
    return this.characterService.getCharactersByUser(queryParams.userId);
  }

  @Get('skill')
  getSkills(@Query() queryParams: { characterId: number }) {
    return this.characterService.getSkillsByCharacter(queryParams.characterId);
  }

  @Get('stat')
  getStats(@Query() queryParams: { characterId: number }) {
    return this.characterService.getStatsByCharacter(queryParams.characterId);
  }

  @Delete('equipment')
  removeCharacterEquipment(@Query() queryParams: { equipmentId: number }) {
    return this.characterService.removeCharacterEquipment(
      queryParams.equipmentId,
    );
  }

  @Get('statusEffects')
  getStatusEffects(@Query() queryParams: { characterId: number }) {
    return this.characterService.getStatusEffectsByCharacter(
      queryParams.characterId,
    );
  }

  @Post('statusEffects')
  insertCharacterStatusEffects(
    @Body() createCharacterStatusEffectDto: CreateCharacterStatusEffectDto,
  ) {
    return this.characterService.insertCharacterStatusEffect(
      createCharacterStatusEffectDto,
    );
  }

  @Delete('statusEffects')
  removeCharacterStatusEffect(
    @Query() queryParams: { characterStatusEffectId: number },
  ) {
    return this.characterService.removeCharacterStatusEffect(
      queryParams.characterStatusEffectId,
    );
  }

  @Delete('item')
  removeCharacterItem(@Query() queryParams: { characterItemId: number }) {
    return this.characterService.removeCharacterItem(
      queryParams.characterItemId,
    );
  }
}
