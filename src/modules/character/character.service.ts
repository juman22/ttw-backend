import { Injectable } from '@nestjs/common';
import CreateCharacterDto from 'src/models/dtos/CreateCharacterDto';
import CreateCharacterSkillDto from 'src/models/dtos/CreateCharacterSkillDto';
import CreateCharacterStatDto from 'src/models/dtos/CreateCharacterStatDto';
import CreateCharacterStatusEffectDto from 'src/models/dtos/CreateCharacterStatusEffectDto';
import UpdateCharacterEquipmentDto from 'src/models/dtos/UpdateCharacterEquipmentDto';
import InsertCharacterEquipmentDto from 'src/models/dtos/InsertCharacterEquipmentDto';
import UpdateCharacterDto from 'src/models/dtos/UpdateCharacterDto';
import UpdateCharacterSkillDto from 'src/models/dtos/UpdateCharacterSkillDto';
import { CharacterDao } from 'src/persistence/daos/character.dao';
import { CharacterEquipmentDao } from 'src/persistence/daos/character.equipment.dao';
import { CharacterSkillDao } from 'src/persistence/daos/character.skill.dao';
import { CharacterStatDao } from 'src/persistence/daos/character.stat.dao';
import { CharacterEffectDao } from 'src/persistence/daos/character.effect.dao';
import CharacterEquipment from 'src/persistence/models/character/character.equipment';
import CharacterSkill from 'src/persistence/models/character/character.skill.model';
import CharacterStat from 'src/persistence/models/character/character.stat';
import CharacterEffect from 'src/persistence/models/character/character.effect';
import { CharacterItemDao } from 'src/persistence/daos/character.item.dao';
import InsertCharacterItemDto from 'src/models/dtos/InsertCharacterItemDto';
import CharacterItem from 'src/persistence/models/character/character.item';
import UpdateCharacterItemDto from 'src/models/dtos/UpdateCharacterItemDto';

@Injectable()
export class CharacterService {
  constructor(
    private readonly characterDao: CharacterDao,
    private readonly characterSkillDao: CharacterSkillDao,
    private readonly characterStatDao: CharacterStatDao,
    private readonly characterEquipmentDao: CharacterEquipmentDao,
    private readonly characterItemDao: CharacterItemDao,
    private readonly characterStatusEffectDao: CharacterEffectDao,
  ) {}

  async getCharactersByUser(userId: number) {
    return this.characterDao.getCharactersByUser(userId);
  }

  async getCharacter(characterId: number) {
    return this.characterDao.getCharacter(characterId);
  }

  async insertCharacter(character: CreateCharacterDto): Promise<Object> {
    console.log(`RECIEVED CHARACTER FOR INSERT ${JSON.stringify(character)}`);
    return this.characterDao.insertCharacter(character);
  }

  async updateCharacter(character: UpdateCharacterDto): Promise<Object> {
    console.log(`RECIEVED CHARACTER FOR UPDATE ${JSON.stringify(character)}`);
    return this.characterDao.updateCharacter(character);
  }

  async insertSkills(skills: CreateCharacterSkillDto[]) {
    const insertedSkills = [];
    skills.forEach(async (skill) => {
      await new Promise<void>(async (resolve) => {
        const response = await this.insertSkill(skill);
        console.log(
          `Recieved response ${JSON.stringify(
            response,
          )} for skill ${JSON.stringify(skill)})}`,
        );
        insertedSkills.push(response);
        resolve();
      });
    });
    return insertedSkills;
  }

  private async insertSkill(characterSkillDto: CreateCharacterSkillDto) {
    const newSkill = new CharacterSkill();
    newSkill.skill = characterSkillDto.skill;
    newSkill.character = characterSkillDto.character;
    newSkill.experience = characterSkillDto.experience;
    const id = await this.characterSkillDao.insertCharacterSkill(newSkill);
    newSkill.id = id;
    return newSkill;
  }

  async updateSkill(
    characterSkillDto: UpdateCharacterSkillDto,
  ): Promise<Object> {
    const existingSkill = await this.characterSkillDao.getCharacterSkill(
      characterSkillDto.id,
    );

    if (existingSkill) {
      existingSkill.experience = characterSkillDto.experience;
      return this.characterSkillDao.updateCharacterSkill(existingSkill);
    } else {
      console.log(`No existing skill found for ${characterSkillDto.id}`);
    }
  }

  async getEquipmentByCharacterId(characterId: number) {
    return this.characterEquipmentDao.getCharacterEquipmentByCharacter(
      characterId,
    );
  }

  async getItemsByCharacterId(characterId: number) {
    return this.characterItemDao.getCharacterItemByCharacter(characterId);
  }

  async insertCharacterEquipment(
    insertCharacterEquipmentDto: InsertCharacterEquipmentDto,
  ) {
    const newCharacterEquipment = new CharacterEquipment();
    newCharacterEquipment.character = insertCharacterEquipmentDto.character;
    newCharacterEquipment.equipment = insertCharacterEquipmentDto.equipment;
    newCharacterEquipment.isEquipped = false;
    const id = await this.characterEquipmentDao.insertCharacterEquipment(
      newCharacterEquipment,
    );
    newCharacterEquipment.id = id;
    return newCharacterEquipment;
  }

  async insertCharacterItem(insertCharacterItemDto: InsertCharacterItemDto) {
    const newCharacterItem = new CharacterItem();
    newCharacterItem.character = insertCharacterItemDto.character;
    newCharacterItem.item = insertCharacterItemDto.item;
    newCharacterItem.quantity = 1;
    const id = await this.characterItemDao.insertCharacterItem(
      newCharacterItem,
    );
    newCharacterItem.id = id;
    return newCharacterItem;
  }

  async insertCharacterStatusEffect(
    createCharacterStatusEffectDto: CreateCharacterStatusEffectDto,
  ) {
    const newCharacterStatusEffect = new CharacterEffect();
    newCharacterStatusEffect.character =
      createCharacterStatusEffectDto.character;
    newCharacterStatusEffect.statusEffect =
      createCharacterStatusEffectDto.statusEffect;
    const id = await this.characterStatusEffectDao.insertCharacterEffect(
      newCharacterStatusEffect,
    );
    newCharacterStatusEffect.id = id;
    return newCharacterStatusEffect;
  }

  async removeCharacterEquipment(characterEquipmentId: number) {
    return this.characterEquipmentDao.removeCharacterEquipmentById(
      characterEquipmentId,
    );
  }

  async removeCharacterStatusEffect(characterStatusEffectId: number) {
    return this.characterStatusEffectDao.removeCharacterEffectById(
      characterStatusEffectId,
    );
  }

  async removeCharacterItem(characterItemId: number) {
    return this.characterItemDao.removeCharacterItemById(characterItemId);
  }

  async equipItem(updateEquipmentDto: UpdateCharacterEquipmentDto) {
    return this.updateEquipment(updateEquipmentDto, true);
  }

  async unequipItem(updateEquipmentDto: UpdateCharacterEquipmentDto) {
    return this.updateEquipment(updateEquipmentDto, false);
  }

  async updateEquipment(
    updateEquipmentDto: UpdateCharacterEquipmentDto,
    isEquipped: boolean,
  ) {
    const existingItem = await this.characterEquipmentDao.getCharacterEquipmentById(
      updateEquipmentDto.id,
    );

    if (existingItem) {
      existingItem.isEquipped = isEquipped;
      const response = await this.characterEquipmentDao.updateCharacterEquipment(
        existingItem,
      );
      console.log(`Item update response : ${JSON.stringify(response)}`);
    } else {
      console.log(`No existing item found for ${updateEquipmentDto.id}`);
    }
  }

  async updateCharacterItem(updateCharacterItemDto: UpdateCharacterItemDto) {
    const existingItem = await this.characterItemDao.getCharacterItemById(
      updateCharacterItemDto.id,
    );

    if (existingItem) {
      existingItem.quantity = updateCharacterItemDto.quantity;
      const response = await this.characterItemDao.updateCharacterItem(
        existingItem,
      );
      console.log(`Item update response : ${JSON.stringify(response)}`);
    } else {
      console.log(`No existing item found for ${updateCharacterItemDto.id}`);
    }
  }

  async insertStat(statDto: CreateCharacterStatDto) {
    const newStat = new CharacterStat();
    newStat.character = statDto.character;
    newStat.stat = statDto.stat;
    newStat.value = statDto.value;
    const id = await this.characterStatDao.insertCharacterStat(newStat);
    newStat.id = id;
    return newStat;
  }

  async insertStats(stats: CreateCharacterStatDto[]) {
    console.log(`REC STATS: ${JSON.stringify(stats)}`);
    const insertedStats = [];
    stats.forEach(async (stat) => {
      await new Promise<void>(async (resolve) => {
        const response = await this.insertStat(stat);
        console.log(
          `Recieved response ${JSON.stringify(
            response,
          )} for stat ${JSON.stringify(stat)})}`,
        );
        insertedStats.push(response);
        resolve();
      });
    });
    return insertedStats;
  }

  async getSkillsByCharacter(characterId: number) {
    console.log(`skill id : ${characterId}`);
    return this.characterSkillDao.getCharacterSkillsByCharacter(characterId);
  }

  async getStatsByCharacter(characterId: number) {
    console.log(`stat id : ${characterId}`);

    return this.characterStatDao.getCharacterStatsByCharacter(characterId);
  }

  async getStatusEffectsByCharacter(characterId: number) {
    const response = await this.characterStatusEffectDao.getCharacterEffectByCharacter(
      characterId,
    );
    //console.log(`status effects: ${JSON.stringify(response)}`);
    return response;
  }
}
