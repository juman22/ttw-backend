import Character from 'src/persistence/models/character/character.model';
import Item from 'src/persistence/models/setting/item.model';

export default class InsertCharacterEquipmentDto {
  item: Item;
  character: Character;
}
