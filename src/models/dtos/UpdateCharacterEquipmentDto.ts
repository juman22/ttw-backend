import Character from 'src/persistence/models/character/character.model';
import Equipment from 'src/persistence/models/setting/equipment.model';

export default class UpdateCharacterEquipmentDto {
  id: number;
  equipment: Equipment;
  character: Character;
}
