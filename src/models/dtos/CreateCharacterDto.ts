import Class from 'src/persistence/models/setting/class.model';
import Race from 'src/persistence/models/setting/race.model';
import Setting from 'src/persistence/models/setting/setting.model';
import User from 'src/persistence/models/user/user.model';

export default class CreateCharacterDto {
  name: string;
  user: User;
  setting: Setting;
  race: Race;
  clazz: Class;
  maxHP: number;
}
