import Character from 'src/persistence/models/character/character.model';
import Skill from 'src/persistence/models/setting/skill.model';

export default class CreateCharacterSkillDto {
  skill: Skill;
  character: Character;
  experience: number;
}
