import Character from 'src/persistence/models/character/character.model';
import Stat from 'src/persistence/models/setting/stat.model';

export default class CreateCharacterStatDto {
  character: Character;
  stat: Stat;
  value: number;
}
