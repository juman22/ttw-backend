import Character from 'src/persistence/models/character/character.model';
import Item from 'src/persistence/models/setting/item.model';

export default class UpdateCharacterItemDto {
  id: number;
  item: Item;
  character: Character;
  quantity: number;
}
