import Class from 'src/persistence/models/setting/class.model';
import Race from 'src/persistence/models/setting/race.model';
import Setting from 'src/persistence/models/setting/setting.model';
import User from 'src/persistence/models/user/user.model';

export default class UpdateCharacterDto {
  id: number;
  name: string;
  user: User;
  setting: Setting;
  race: Race;
  clazz: Class;
  hp: number;
  endurantCharges: number;
  toxicity: number;
  hunger: number;
}
