import Character from 'src/persistence/models/character/character.model';
import StatusEffect from 'src/persistence/models/setting/status.effect.model';

export default class CreateCharacterStatusEffectDto {
  character: Character;
  statusEffect: StatusEffect;
}
