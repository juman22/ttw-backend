import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { getManager } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PreauthMiddleware } from './auth/preauth.middleware';
import { CharacterModule } from './modules/character/character.module';
import { SettingModule } from './modules/setting/setting.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [UserModule, CharacterModule, SettingModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(PreauthMiddleware).forRoutes({
      path: '*',
      method: RequestMethod.ALL,
    });
  }
  onApplicationShutdown() {
    console.log('Closing PG connection');
    getManager().connection.close();
  }
}
