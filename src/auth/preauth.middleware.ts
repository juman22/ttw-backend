import { Injectable, NestMiddleware } from '@nestjs/common';
import * as firebase from 'firebase-admin';
import * as serviceAccount from './table-top-world-firebase-adminsdk-87xli-926582d181.json';
import { Request, Response, NextFunction } from 'express';

const firebase_params = {
  type: serviceAccount.type,
  projectId: serviceAccount.project_id,
  privateKeyId: serviceAccount.private_key_id,
  privateKey: serviceAccount.private_key,
  clientEmail: serviceAccount.client_email,
  clientId: serviceAccount.client_id,
  authUri: serviceAccount.auth_uri,
  tokenUri: serviceAccount.token_uri,
  authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
  clientC509CertUrl: serviceAccount.client_x509_cert_url,
};

Injectable();
export class PreauthMiddleware implements NestMiddleware {
  private defaultApp: any;

  constructor() {
    this.defaultApp = firebase.initializeApp({
      credential: firebase.credential.cert(firebase_params),
    });
  }
  async use(req: Request, res: Response, next: NextFunction) {
    const token = req.header('Authorization').replace('Bearer', '').trim();
    if (token != null && token != '') {
      try {
        const decodedToken = await this.defaultApp
          .auth()
          .verifyIdToken(token.replace('Bearer', ''));
        const user = {
          email: decodedToken.email,
        };
        req['user'] = user;
        next();
        return;
      } catch (error) {
        console.error(`${error}`);
        this.accessDenied(req.url, res);
      }
    }
    next();
  }

  private accessDenied(url: string, res: Response) {
    res.status(403).json({
      statusCode: 403,
      timestamp: new Date().toISOString(),
      path: url,
      message: 'Access Denied',
    });
  }
}
