import { NestFactory } from '@nestjs/core';
import { createConnection } from 'typeorm';
import { AppModule } from './app.module';
import { typeOrmConfig } from './config';

async function bootstrap() {
  await require('trace-unhandled/register');
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.enableShutdownHooks();
  console.log('Opening PG connection');
  await createConnection(typeOrmConfig);
  await app.listen(3000);
}
bootstrap();
