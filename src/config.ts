import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import Character from './persistence/models/character/character.model';
import CharacterSkill from './persistence/models/character/character.skill.model';
import Class from './persistence/models/setting/class.model';
import Setting from './persistence/models/setting/setting.model';
import Race from './persistence/models/setting/race.model';
import Skill from './persistence/models/setting/skill.model';
import User from './persistence/models/user/user.model';
import CharacterStat from './persistence/models/character/character.stat';
import Stat from './persistence/models/setting/stat.model';
import Equipment from './persistence/models/setting/equipment.model';
import StatModifier from './persistence/models/setting/stat.modifier.model';
import CharacterEquipment from './persistence/models/character/character.equipment';
import StatusEffect from './persistence/models/setting/status.effect.model';
import CharacterEffect from './persistence/models/character/character.effect';
import SkillModifier from './persistence/models/setting/skill.modifier.model';
import UniversalModifier from './persistence/models/setting/universal.modifier';
import CharacterItem from './persistence/models/character/character.item';
import Item from './persistence/models/setting/item.model';

const typeOrmConfig: PostgresConnectionOptions = {
  type: 'postgres',
  host: 'database',
  port: 5432,
  username: 'typeormtest',
  password: 'password',
  database: 'typeormtest',
  synchronize: true,
  logging: false,
  entities: [
    User,
    Character,
    CharacterSkill,
    Setting,
    Skill,
    Class,
    Race,
    Stat,
    CharacterStat,
    Equipment,
    StatModifier,
    SkillModifier,
    CharacterEquipment,
    StatusEffect,
    CharacterEffect,
    UniversalModifier,
    Item,
    CharacterItem,
  ],
};

export { typeOrmConfig };
