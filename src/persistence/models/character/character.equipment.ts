import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Equipment from '../setting/equipment.model';
import Character from './character.model';

@Entity()
export default class CharacterEquipment {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Character)
  character;

  @ManyToOne(() => Equipment)
  equipment;

  @Column()
  isEquipped: boolean;
}
