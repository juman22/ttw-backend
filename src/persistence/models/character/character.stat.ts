import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Stat from '../setting/stat.model';
import Character from './character.model';

@Entity()
export default class CharacterStat {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Character)
  character;

  @ManyToOne(() => Stat)
  stat;

  @Column()
  value: number;
}
