import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import StatusEffect from '../setting/status.effect.model';
import Character from './character.model';

@Entity()
export default class CharacterEffect {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Character)
  character;

  @ManyToOne(() => StatusEffect)
  statusEffect;
}
