import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Item from '../setting/item.model';
import Character from './character.model';

@Entity()
export default class CharacterItem {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Character)
  character;

  @ManyToOne(() => Item)
  item;

  @Column()
  quantity: number;
}
