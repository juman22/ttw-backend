import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Class from '../setting/class.model';
import Race from '../setting/race.model';
import Setting from '../setting/setting.model';
import User from '../user/user.model';
import CharacterEquipment from './character.equipment';
import CharacterSkill from './character.skill.model';
import CharacterStat from './character.stat';
import CharacterEffect from './character.effect';

@Entity()
export default class Character {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('text', {default: ""})
  notes: string;

  @Column()
  hp: number;

  @Column()
  maxHp: number;

  @Column()
  endurantCharges: number;

  @Column({default: 0})
  toxicity: number;

  @Column({default: 0})
  hunger: number;

  @ManyToOne(() => User)
  user: User;

  @ManyToOne(() => Setting)
  setting: Setting;

  @ManyToOne(() => Race)
  race: Race;

  @ManyToOne(() => Class)
  clazz: Class;

  @OneToMany(() => CharacterSkill, (characterSkill) => characterSkill.character)
  skills: CharacterSkill[];

  @OneToMany(() => CharacterStat, (characterStat) => characterStat.character)
  stats: CharacterStat[];

  @OneToMany(
    () => CharacterEquipment,
    (characterEquipment) => characterEquipment.character,
  )
  equipment: CharacterEquipment[];

  @OneToMany(
    () => CharacterEffect,
    (characterStatusEffect) => characterStatusEffect.character,
  )
  characterStatusEffect: CharacterEffect[];
}
