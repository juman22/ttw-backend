import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Skill from '../setting/skill.model';
import Character from './character.model';

@Entity()
export default class CharacterSkill {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Character)
  character;

  @ManyToOne(() => Skill)
  skill;

  @Column()
  experience: number;
}
