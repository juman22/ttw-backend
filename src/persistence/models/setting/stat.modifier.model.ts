import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Stat from './stat.model';

@Entity()
export default class StatModifier {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Stat)
  stat: Stat;

  @Column()
  value: number;
}
