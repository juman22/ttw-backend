import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import StatModifier from './stat.modifier.model';
import Setting from './setting.model';
import SkillModifier from './skill.modifier.model';
import UniversalModifier from './universal.modifier';

@Entity()
export default class StatusEffect {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Setting)
  setting: Setting;

  @ManyToOne(() => UniversalModifier)
  universalModifier: UniversalModifier;

  @ManyToMany(() => StatModifier)
  @JoinTable()
  statModifiers: StatModifier[];

  @ManyToMany(() => SkillModifier)
  @JoinTable()
  skillModifiers: SkillModifier[];
}
