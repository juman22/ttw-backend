import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class UniversalModifier {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  hp: number;

  @Column()
  armorClass: number;

  @Column()
  movementSpeed: number;

  @Column()
  overdose: number;

  @Column()
  universal: number;
}
