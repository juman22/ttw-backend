import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Setting from './setting.model';
import Stat from './stat.model';

@Entity()
export default class Skill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Setting)
  setting: Setting;

  @ManyToOne(() => Stat)
  stat: Stat;
}
