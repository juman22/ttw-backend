import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Setting from './setting.model';

@Entity()
export default class Race {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Setting)
  setting: Setting;
}
