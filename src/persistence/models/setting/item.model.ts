import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Setting from './setting.model';

@Entity()
export default class Item {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('varchar', { length: 200 })
  notes: string;

  @ManyToOne(() => Setting)
  setting: Setting;
}
