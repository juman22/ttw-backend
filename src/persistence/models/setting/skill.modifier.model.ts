import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Skill from './skill.model';

@Entity()
export default class SkillModifier {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Skill)
  skill: Skill;

  @Column()
  value: number;
}
