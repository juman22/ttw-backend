import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import Race from '../models/setting/race.model';

@Injectable()
export class RaceDao {
  public async getAllRaces() {
    const raceRepository = getRepository(Race);

    return raceRepository.find();
  }

  public async getRacesBySetting(settingId: number) {
    const raceRepository = getRepository(Race);

    return raceRepository.find({
      where: {
        setting: settingId,
      },
    });
  }
}
