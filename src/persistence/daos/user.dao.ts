import { Injectable } from '@nestjs/common';
import CreateUserDto from 'src/models/dtos/CreateUserDto';
import { getRepository } from 'typeorm';
import User from '../models/user/user.model';

@Injectable()
export class UserDao {
  public async insertUser(user: CreateUserDto): Promise<Object> {
    const userRepository = getRepository(User);

    const result = await userRepository.insert(user);
    return result.identifiers[0];
  }

  public async getUser(uid: string) {
    const userRepository = getRepository(User);

    const result = await userRepository.findOne({
      where: {
        uid: uid,
      },
    });

    console.log(`RESULT: ${result}`);
    return result;
  }
}
