import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import CharacterSkill from '../models/character/character.skill.model';

@Injectable()
export class CharacterSkillDao {
  public async insertCharacterSkill(
    characterSkill: CharacterSkill,
  ): Promise<number> {
    const characterSkillRepository = getRepository(CharacterSkill);

    const result = await characterSkillRepository.insert(characterSkill);
    return result.identifiers[0].id;
  }

  public async getCharacterSkill(id: number) {
    const characterSkillRepository = getRepository(CharacterSkill);

    return characterSkillRepository.findOne({
      where: {
        id: id,
      },
    });
  }

  public async updateCharacterSkill(characterSkill: CharacterSkill) {
    const characterSkillRepository = getRepository(CharacterSkill);

    const result = await characterSkillRepository.save(characterSkill);
    return result;
  }

  public async getCharacterSkillsByCharacter(characterId: number) {
    const characterSkillRepository = getRepository(CharacterSkill);

    return characterSkillRepository.find({
      where: {
        character: characterId,
      },
      relations: ['skill', 'skill.stat'],
    });
  }
}
