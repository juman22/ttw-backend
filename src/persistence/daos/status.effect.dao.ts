import { Injectable } from '@nestjs/common';
import { getRepository, ILike } from 'typeorm';
import StatusEffect from '../models/setting/status.effect.model';

@Injectable()
export class StatusEffectDao {
  public async getAllStatusEffects() {
    const statusEffectRepository = getRepository(StatusEffect);

    return statusEffectRepository.find();
  }

  public async getStatusEffectsBySetting(settingId: number) {
    const statusEffectRepository = getRepository(StatusEffect);

    return statusEffectRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async searchStatusEffectsBySetting(
    settingId: number,
    searchString: string,
  ) {
    const statusEffectRepository = getRepository(StatusEffect);

    return statusEffectRepository.find({
      where: {
        setting: settingId,
        name: ILike(`%${searchString}%`),
      },
      select: ['name', 'id'],
    });
  }

  public async insertStatusEffect(statusEffect: StatusEffect) {
    const statusEffectRepository = getRepository(StatusEffect);

    return statusEffectRepository.save(statusEffect);
  }
}
