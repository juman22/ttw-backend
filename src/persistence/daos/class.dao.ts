import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import Class from '../models/setting/class.model';

@Injectable()
export class ClassDao {
  public async getAllClasses() {
    const classRepository = getRepository(Class);

    return classRepository.find();
  }

  public async getClassesBySetting(settingId: number) {
    const classRepository = getRepository(Class);

    return classRepository.find({
      where: {
        setting: settingId,
      },
    });
  }
}
