import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import CharacterStat from '../models/character/character.stat';

@Injectable()
export class CharacterStatDao {
  public async insertCharacterStat(
    characterStat: CharacterStat,
  ): Promise<number> {
    const characterStatRepository = getRepository(CharacterStat);

    const result = await characterStatRepository.insert(characterStat);
    return result.identifiers[0].id;
  }

  public async getCharacterStat(id: number) {
    const characterStatRepository = getRepository(CharacterStat);

    return characterStatRepository.findOne({
      where: {
        id: id,
      },
    });
  }

  public async updateCharacterStat(characterStat: CharacterStat) {
    const characterStatRepository = getRepository(CharacterStat);

    const result = await characterStatRepository.save(characterStat);
    return result;
  }

  public async getCharacterStatsByCharacter(characterId: number) {
    const characterStatRepository = getRepository(CharacterStat);

    return characterStatRepository.find({
      where: {
        character: characterId,
      },
      relations: ['stat'],
    });
  }
}
