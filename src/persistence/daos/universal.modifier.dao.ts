import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import UniversalModifier from '../models/setting/universal.modifier';

@Injectable()
export class UniversalModifierDao {
  public async getAllModifiers() {
    const unviersalModifierRepository = getRepository(UniversalModifier);

    return unviersalModifierRepository.find();
  }

  public async getModifiersBySetting(settingId: number) {
    const unviersalModifierRepository = getRepository(UniversalModifier);

    return unviersalModifierRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async insertUniversalModifiers(modifiers: UniversalModifier[]) {
    const unviersalModifierRepository = getRepository(UniversalModifier);

    return unviersalModifierRepository.save(modifiers);
  }

  public async insertUniversalModifier(modifier: UniversalModifier) {
    const unviersalModifierRepository = getRepository(UniversalModifier);

    return unviersalModifierRepository.save(modifier);
  }
}
