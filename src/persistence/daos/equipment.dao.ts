import { Injectable } from '@nestjs/common';
import { getRepository, ILike } from 'typeorm';
import Equipment from '../models/setting/equipment.model';

@Injectable()
export class EquipmentDao {
  public async getAllEquipments() {
    const equipmentRepository = getRepository(Equipment);

    return equipmentRepository.find();
  }

  public async getEquipmentsBySetting(settingId: number) {
    const equipmentRepository = getRepository(Equipment);

    return equipmentRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async searchEquipmentBySetting(
    settingId: number,
    searchString: string,
  ) {
    const equipmentRepository = getRepository(Equipment);

    return equipmentRepository.find({
      where: {
        setting: settingId,
        name: ILike(`%${searchString}%`),
      },
      select: ['name', 'id'],
    });
  }

  public async insertEquipment(equipment: Equipment) {
    const equipmentRepository = getRepository(Equipment);

    return equipmentRepository.save(equipment);
  }
}
