import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import SkillModifier from '../models/setting/skill.modifier.model';

@Injectable()
export class SkillModifierDao {
  public async getAllModifiers() {
    const skillModifierRepository = getRepository(SkillModifier);

    return skillModifierRepository.find();
  }

  public async getModifiersBySetting(settingId: number) {
    const skillModifierRepository = getRepository(SkillModifier);

    return skillModifierRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async insertSkillModifiers(modifiers: SkillModifier[]) {
    const skillModifierRepository = getRepository(SkillModifier);

    return skillModifierRepository.save(modifiers);
  }
}
