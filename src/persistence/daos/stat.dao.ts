import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import Stat from '../models/setting/stat.model';

@Injectable()
export class StatDao {
  public async getAllStats() {
    const statRepository = getRepository(Stat);

    return statRepository.find();
  }

  public async getStatsBySetting(settingId: number) {
    const statRepository = getRepository(Stat);

    return statRepository.find({
      where: {
        setting: settingId,
      },
    });
  }
}
