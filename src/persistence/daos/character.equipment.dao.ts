import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import CharacterEquipment from '../models/character/character.equipment';

@Injectable()
export class CharacterEquipmentDao {
  public async insertCharacterEquipment(
    characterEquipment: CharacterEquipment,
  ): Promise<number> {
    const characterEquipmentRepository = getRepository(CharacterEquipment);

    const result = await characterEquipmentRepository.insert(
      characterEquipment,
    );
    return result.identifiers[0].id;
  }

  public async getCharacterEquipmentById(id: number) {
    const characterEquipmentRepository = getRepository(CharacterEquipment);

    return characterEquipmentRepository.findOne({
      where: {
        id: id,
      },
      relations: [
        'equipment',
        'equipment.universalModifier',
        'equipment.statModifiers',
        'equipment.statModifiers.stat',
        'equipment.skillModifiers',
        'equipment.skillModifiers.skill',
      ],
    });
  }

  public async updateCharacterEquipment(
    characterEquipment: CharacterEquipment,
  ) {
    const characterEquipmentRepository = getRepository(CharacterEquipment);

    const result = await characterEquipmentRepository.save(characterEquipment);
    return result;
  }

  public async getCharacterEquipmentByCharacter(characterId: number) {
    const characterEquipmentRepository = getRepository(CharacterEquipment);

    return characterEquipmentRepository.find({
      where: {
        character: characterId,
      },
      relations: [
        'equipment',
        'equipment.universalModifier',
        'equipment.statModifiers',
        'equipment.statModifiers.stat',
        'equipment.skillModifiers',
        'equipment.skillModifiers.skill',
      ],
    });
  }

  public async removeCharacterEquipmentById(equipmentId: number) {
    const characterEquipmentRepository = getRepository(CharacterEquipment);

    return characterEquipmentRepository.delete({
      id: equipmentId,
    });
  }
}
