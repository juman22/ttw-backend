import { Injectable } from '@nestjs/common';
import { getRepository, ILike } from 'typeorm';
import Item from '../models/setting/item.model';

@Injectable()
export class ItemDao {
  public async getAllItems() {
    const itemRepository = getRepository(Item);

    return itemRepository.find();
  }

  public async getItemsBySetting(settingId: number) {
    const itemRepository = getRepository(Item);

    return itemRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async searchItemsBySetting(settingId: number, searchString: string) {
    const itemRepository = getRepository(Item);

    return itemRepository.find({
      where: {
        setting: settingId,
        name: ILike(`%${searchString}%`),
      },
      select: ['name', 'id'],
    });
  }

  public async insertItem(item: Item) {
    const itemRepository = getRepository(Item);

    return itemRepository.save(item);
  }
}
