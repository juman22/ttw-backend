import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import StatModifier from '../models/setting/stat.modifier.model';

@Injectable()
export class StatModifierDao {
  public async getAllModifiers() {
    const statModifierRepository = getRepository(StatModifier);

    return statModifierRepository.find();
  }

  public async getModifiersBySetting(settingId: number) {
    const statModifierRepository = getRepository(StatModifier);

    return statModifierRepository.find({
      where: {
        setting: settingId,
      },
    });
  }

  public async insertStatModifiers(modifiers: StatModifier[]) {
    const statModifierRepository = getRepository(StatModifier);

    return statModifierRepository.save(modifiers);
  }
}
