import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import CharacterEffect from '../models/character/character.effect';

@Injectable()
export class CharacterEffectDao {
  public async insertCharacterEffect(
    characterEffect: CharacterEffect,
  ): Promise<number> {
    const characterEffectRepository = getRepository(CharacterEffect);

    const result = await characterEffectRepository.insert(characterEffect);
    return result.identifiers[0].id;
  }

  public async getCharacterEffectById(id: number) {
    const characterEffectRepository = getRepository(CharacterEffect);

    return characterEffectRepository.findOne({
      where: {
        id: id,
      },
      relations: [
        'statusEffect',
        'statusEffect.universalModifier',
        'statusEffect.statModifiers',
        'statusEffect.statModifiers.stat',
        'statusEffect.skillModifiers',
        'statusEffect.skillModifiers.skill',
      ],
    });
  }

  public async updateCharacterEffect(characterEffect: CharacterEffect) {
    const characterEffectRepository = getRepository(CharacterEffect);

    const result = await characterEffectRepository.save(characterEffect);
    return result;
  }

  public async getCharacterEffectByCharacter(characterId: number) {
    const characterEffectRepository = getRepository(CharacterEffect);

    return characterEffectRepository.find({
      where: {
        character: characterId,
      },
      relations: [
        'statusEffect',
        'statusEffect.universalModifier',
        'statusEffect.statModifiers',
        'statusEffect.statModifiers.stat',
        'statusEffect.skillModifiers',
        'statusEffect.skillModifiers.skill',
      ],
    });
  }

  public async removeCharacterEffectById(characterEffectId: number) {
    const characterEffectRepository = getRepository(CharacterEffect);

    return characterEffectRepository.delete({
      id: characterEffectId,
    });
  }
}
