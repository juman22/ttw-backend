import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import Setting from '../models/setting/setting.model';

@Injectable()
export class SettingDao {
  public async getAllSettings() {
    const settingRepository = getRepository(Setting);

    return settingRepository.find();
  }
}
