import { Injectable } from '@nestjs/common';
import CreateCharacterDto from 'src/models/dtos/CreateCharacterDto';
import UpdateCharacterDto from 'src/models/dtos/UpdateCharacterDto';
import { getRepository } from 'typeorm';
import Character from '../models/character/character.model';

@Injectable()
export class CharacterDao {
  public async insertCharacter(character: CreateCharacterDto): Promise<Object> {
    const characterRepository = getRepository(Character);
    const result = await characterRepository.insert(character);
    return result.identifiers[0];
  }

  public async updateCharacter(character: UpdateCharacterDto) {
    const characterSkillRepository = getRepository(Character);

    const result = await characterSkillRepository.save(character);
    return result;
  }

  public async getCharacter(characterId: number) {
    const characterRepository = getRepository(Character);

    return characterRepository.findOne({
      where: {
        id: characterId,
      },
      relations: ['setting', 'race', 'clazz', 'skills'],
    });
  }

  public async getCharactersByUser(userId: number) {
    const characterRepository = getRepository(Character);

    return characterRepository.find({
      where: {
        user: userId,
      },
      relations: ['setting', 'race', 'clazz', 'skills'],
    });
  }
}
