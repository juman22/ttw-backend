import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import Skill from '../models/setting/skill.model';

@Injectable()
export class SkillDao {
  public async getAllSkills() {
    const skillRepository = getRepository(Skill);

    return skillRepository.find();
  }

  public async getSkillsBySetting(settingId: number) {
    const skillRepository = getRepository(Skill);

    return skillRepository.find({
      where: {
        setting: settingId,
      },
    });
  }
}
