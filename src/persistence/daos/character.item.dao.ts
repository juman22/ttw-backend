import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import CharacterItem from '../models/character/character.item';

@Injectable()
export class CharacterItemDao {
  public async insertCharacterItem(
    characterItem: CharacterItem,
  ): Promise<number> {
    const characterItemRepository = getRepository(CharacterItem);

    const result = await characterItemRepository.insert(characterItem);
    return result.identifiers[0].id;
  }

  public async getCharacterItemById(id: number) {
    const characterItemRepository = getRepository(CharacterItem);

    return characterItemRepository.findOne({
      where: {
        id: id,
      },
      relations: ['item'],
    });
  }

  public async updateCharacterItem(characterItem: CharacterItem) {
    const characterItemRepository = getRepository(CharacterItem);

    const result = await characterItemRepository.save(characterItem);
    return result;
  }

  public async getCharacterItemByCharacter(characterId: number) {
    const characterItemRepository = getRepository(CharacterItem);

    return characterItemRepository.find({
      where: {
        character: characterId,
      },
      relations: ['item'],
    });
  }

  public async removeCharacterItemById(itemId: number) {
    const characterItemRepository = getRepository(CharacterItem);

    return characterItemRepository.delete({
      id: itemId,
    });
  }
}
